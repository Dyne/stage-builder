# Only allow architectures supported by debian
case ${arch} in
    amd64)
        echo "${arch} unsupported, use x86_64"
        exit 1
        ;;
    armhf)
        echo "${arch} unsupported, use armv7l"
        exit 1
        ;;
    arm64|arm64v8)
        echo "${arch} unsupported, use aarch64"
        exit 1
        ;;
esac

# Translate the debian architecture into linux ones
case ${arch} in
    x86_64)
        debian_arch="amd64"
        docker_arch="${debian_arch}"
        ;;
    aarch64)
        debian_arch="arm64"
        docker_arch="${arch}"
        ;;
    armv7l)
        debian_arch="armhf"
        docker_arch="arm"
        ;;
    *)
        echo "Unsupported architecture."
        exit 0
        ;;
esac
