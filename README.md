# stage-builder

stage-builder is a collection of scripts for building Devuan stages and live
ISOs. It is based on the knowledge and testing built over the year in the
[Devuan SDK].

[Devuan SDK]: https://git.devuan.org/devuan-sdk

## Features

- No root required. Everything is built using fakeroot and docker
- Reproducible build step thanks to container layers
- Small and extendable scripts. All the source is less than 500 LOC

## Dependencies

stage-builder requires the following software to run:

- Docker running as the current user
- fakeroot
- fakechroot
- tar
- wget _\*\*_
- squashfs-tools-ng_\*_
- grub2_\*_

\* (for building the live-iso only)
\** (for cross-building support)

## License

stage-builder is licensed under the GPL-3 license.
